---
#
## NO SECRETS IN THIS FILE!!
#

# environment "aws, esxi, lxc"
env: aws
qa_run: false

awslogs_agent_proxy_port: '{{ squid_port }}'
awslogs_agent_proxy_host: '{{ squid_proxy }}'
rds_instance_type: db.t2.medium
rds_region: '{{ aws_region }}'
multi_zone: true

ansible_ssh_common_args: '-o StrictHostKeyChecking=no'
overwrite_yps_database: true
overwrite_scc_database: true
overwrite_ems_database: true
fetch_vendor_blob: false
enable_licgen_extraction: false

# Granary
log_export_aws_access_key: "{{ aws_access_key_id_for_log_share }}"
log_export_aws_secret_key: "{{ aws_secret_key_for_log_share }}"
#enable_granary: true

ems_s3_path: /emsent/{{ ems_release }}/{{ build_type }}/{{ ems_build }}/EMSEnterpriseForLinux
s3_path: /sc/{{ release }}/{{ build_type }}/{{ build }}/SafeNetSentinelCloudCD

tomcat_jolokia_enabled: true
tomcat_jmxtrans_enabled: true
tomcat_config_remote_log_enabled: true
enable_collectd: true
enable_squid_socks_proxy: true


de_ec2_instance_type: t2.medium
de_proxy_host: '{{ squid_proxy }}'
de_proxy_port: '{{ squid_port }}'

ems_ec2_instance_type: t2.medium
ems_proxy_connect: 2
ems_service_port: '{{ http_port }}'
ems_socks_host: '{{ squid_proxy }}'
ems_socks_port: '{{ socks_port }}'
ems_url_host: '{{ isv_alias }}'
ems_use_socks_server: true

pa_ec2_instance_type: t2.medium

scc_ec2_instance_type: t2.medium
scc_proxy_host: '{{ squid_proxy }}'
scc_proxy_port: '{{ squid_port }}'

ha_ec2_instance_type: m5.large
haproxy_ssl_cert_path: /etc/ssl/certs/_.prod.sentinelcloud.pem


squid_ip: '{{ squid_proxy }}:{{ squid_port }}'
termination_protection: true

yps_ec2_instance_type: t2.medium
ems_sites:
  site1:
    instance_type: '{{ ems_ec2_instance_type }}'
    number_of_instances: 1
    region: '{{ aws_region }}'
    security_group: '{{ ems_ec2_security_group_name }}'
    subnet: '{{ zone_a_app_subnet_id }}'
    zone: '{{ aws_region }}a'
  site2:
    instance_type: '{{ ems_ec2_instance_type }}'
    number_of_instances: 1
    region: '{{ aws_region }}'
    security_group: '{{ ems_ec2_security_group_name }}'
    subnet: '{{ zone_b_app_subnet_id }}'
    zone: '{{ aws_region }}b'
de_sites:
  site1:
    instance_type: '{{ de_ec2_instance_type }}'
    number_of_instances: 1
    region: '{{ aws_region }}'
    security_group: '{{ de_ec2_security_group_name }}'
    subnet: '{{ zone_a_app_subnet_id }}'
    zone: '{{ aws_region }}a'
haproxy_sites:
  site1:
    instance_type: '{{ ha_ec2_instance_type }}'
    number_of_instances: 1
    region: '{{ aws_region }}'
    security_group: '{{ ha_ec2_security_group_name }}'
    subnet: '{{ zone_a_ha_subnet_id }}'
    zone: '{{ aws_region }}a'
  site2:
    instance_type: '{{ ha_ec2_instance_type }}'
    number_of_instances: 1
    region: '{{ aws_region }}'
    security_group: '{{ ha_ec2_security_group_name }}'
    subnet: '{{ zone_b_ha_subnet_id }}'
    zone: '{{ aws_region }}b'
pa_sites:
  site1:
    instance_type: '{{ pa_ec2_instance_type }}'
    number_of_instances: 1
    region: '{{ aws_region }}'
    security_group: '{{ pa_ec2_security_group_name }}'
    subnet: '{{ zone_a_app_subnet_id }}'
    zone: '{{ aws_region }}a'
  site2:
    instance_type: '{{ pa_ec2_instance_type }}'
    number_of_instances: 1
    region: '{{ aws_region }}'
    security_group: '{{ pa_ec2_security_group_name }}'
    subnet: '{{ zone_b_app_subnet_id }}'
    zone: '{{ aws_region }}b'
scc_sites:
  site1:
    instance_type: '{{ scc_ec2_instance_type }}'
    number_of_instances: 1
    region: '{{ aws_region }}'
    security_group: '{{ scc_ec2_security_group_name }}'
    subnet: '{{ zone_a_app_subnet_id }}'
    zone: '{{ aws_region }}a'
  site2:
    instance_type: '{{ scc_ec2_instance_type }}'
    number_of_instances: 1
    region: '{{ aws_region }}'
    security_group: '{{ scc_ec2_security_group_name }}'
    subnet: '{{ zone_b_app_subnet_id }}'
    zone: '{{ aws_region }}b'
yps_sites:
  site1:
    instance_type: '{{ yps_ec2_instance_type }}'
    number_of_instances: 1
    region: '{{ aws_region }}'
    security_group: '{{ yps_ec2_security_group_name }}'
    subnet: '{{ zone_a_app_subnet_id }}'
    zone: '{{ aws_region }}a'
  site2:
    instance_type: '{{ yps_ec2_instance_type }}'
    number_of_instances: 1
    region: '{{ aws_region }}'
    security_group: '{{ yps_ec2_security_group_name }}'
    subnet: '{{ zone_b_app_subnet_id }}'
    zone: '{{ aws_region }}b'
