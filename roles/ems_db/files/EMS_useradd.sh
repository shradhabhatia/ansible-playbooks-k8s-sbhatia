#!/bin/bash
#set -x

# constants
ROOT_NAMESPACE="root"
RESOURCES=("CM" "EM" "LMCM" "RAM" "DM" "DWM")
RO_PERMISSION="V"


function run_query {
    local query="$1"
    mysql -B -h $dbhost -u $dbuser -p${dbpassword} ${dbname} -N -e "$query"
}

function get_role_id {
    local roleName=$1
    local query="select RoleId from t_roles where RoleName = '$roleName';"
    run_query "$query"
}

function add_role {
    local roleName=$1
    local roleDescription="$2"
    local query="insert into t_roles (RoleName, Descr, IsEnabled, SystemRole) values ('$roleName', '$roleDescription', 1, 0);"
    local roleId=$(get_role_id $roleName)
    if [ -z $(get_role_id $roleName) ]; then
        run_query "$query"
#        get_role_id $roleName
    fi
}

function get_perm_id {
    local resourceName=$1
    local permission=${2:-$RO_PERMISSION}
    local query="select PermissionId from t_permission where ResourceName = '$resourceName' and Permission = '$permission';"
    run_query "$query"
}

function add_perm {
    local resourceName=$1
    local permission=${2:-$RO_PERMISSION}
    local query="insert into t_permission (ResourceName, Permission) values ('$resourceName', '$permission');"
    if [ "x$(get_perm_id $resourceName $permission)" == "x" ]; then
        run_query "$query"
#        get_perm_id $resourceName $permission
    fi
}

function add_ro_perms {
    for res in "${RESOURCES[@]}"; do
        local resource="${ROOT_NAMESPACE}/${res}"
        add_perm $resource $RO_PERMISSION
    done
}

function role_has_perm {
    local roleId=$1
    local permissionId=$2
    local query="select * from t_role_permissions_ref where RoleId = '$roleId' and PermissionId = '$permissionId';"
    if [ "x$(run_query "$query")" == "x" ]; then
        echo "false"
    else
        echo "true"
    fi
}

function assign_ro_perms_to_role {
    add_ro_perms
    for res in "${RESOURCES[@]}"; do
        local roleName=$1
        local resource="${ROOT_NAMESPACE}/${res}"
        local permissionId=$(get_perm_id $resource $RO_PERMISSION)
        local roleId=$(get_role_id $roleName)
        local query="insert into t_role_permissions_ref (RoleId, PermissionId) values ('$roleId', '$permissionId');"
        if [ $(role_has_perm $roleId $permissionId) == "false" ]; then
            run_query "$query"
        fi
    done
}

function get_user_id {
    local username=$1
    local query="select UserId from t_user where LoginId = '$username';"
    run_query "$query"
}

function add_user {
    local username=$1
    local emailaddress=$2
    local password=$3
    local query="insert into t_user (LoginId, ExpiresOn, IsEnabled, EmailId, UserPassword, RefId1, RefId2) values ('$username', NULL, 1, '$emailaddress', MD5('$password'), NULL, NULL);"
    local userId=$(get_user_id $username)
    if [ -z $userId ]; then
        run_query "$query"
#        get_user_id $username
    fi
}

function user_has_role {
    local userId=$1
    local roleId=$2
    local query="select * from t_user_roles_ref where UserID = '$userId' and RoleId = '$roleId';"
    local hasrole=$(run_query "$query")
    if [ "x$hasrole" == "x" ]; then
        echo false
    else
        echo true
    fi
}

function assign_role_to_user {
    local roleName=$1
    local username=$2
    local roleId=$(get_role_id $roleName)
    local userId=$(get_user_id $username)
    local query="insert into t_user_roles_ref (UserId, RoleId) values ('$userId', '$roleId');"
    if [ $(user_has_role $userId $roleId) == "false" ]; then
        run_query "$query"
    fi
}

function error {
    local message="$*"
    printf "Error: %s\n" "$message" >&2
    exit 1
}

function help {
    cat <<EOM
Usage: $0 -h DBHOST -u DBUSER -p DBPASS -d DBNAME -U EMSUSER -E EMSEMAIL -P EMSPASS -R ROLENAME -D ROLEDESC
Add an EMS user and Role.

Arguments
    -h  database hostname or IP address
    -u  database username
    -p  database password
    -d  database name
    -U  EMS username
    -E  EMS user email
    -P  EMS password
    -R  role name
    -D  role description (quoted)

Report bugs to ben.nugent@safenet-inc.com
EOM
}

function main {
    add_user $username $emailaddress "$password"
    add_role $roleName "$roleDescription"
    assign_ro_perms_to_role $roleName
    assign_role_to_user $roleName $username
}


while getopts ":h:u:p:d:U:E:P:R:D:" opt; do
    case $opt in
        h)
            dbhost="$OPTARG"
            ;;
        u)
            dbuser="$OPTARG"
            ;;
        p)
            dbpassword="$OPTARG"
            ;;
        d)
            dbname="$OPTARG"
            ;;
        U)
            username="$OPTARG"
            ;;
        E)
            emailaddress="$OPTARG"
            ;;
        P)
            password="$OPTARG"
            ;;
        R)
            roleName="$OPTARG"
            ;;
        D)
            roleDescription="$OPTARG"
            ;;
        \?)
            error "Invalid Option: -$OPTARG"
            ;;
        :)
            error "Missing argument for -$OPTARG"
            ;;
    esac
done

if [ "$dbhost" -a "$dbuser" -a "$dbpassword" -a "$dbname" -a "$username" -a "$emailaddress" -a "$password" -a "$roleName" -a "$roleDescription" ]; then
    main
else
    help
    error "All options are required"
fi
