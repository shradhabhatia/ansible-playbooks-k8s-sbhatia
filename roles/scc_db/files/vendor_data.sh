#!/bin/bash

read_file_by_line()
{

    ## usage: read_file_by_line filename
    ## reads a file into an array of lines; file is expected to exist and be readable
    MYFILE=${1}
    ORIGIFS=${IFS}
    IFS=$(echo -en "\n\b")
    exec 3<&0
    exec 0<"${MYFILE}"
    ITER=0
    while read -r MYLINE || [ -n "${MYLINE}" ]
    do
        ## skip blank lines
        if [[ ! ${MYLINE} =~ "^$" ]]; then
            MYLINES[${ITER}]=${MYLINE}
        fi
        ITER=$((${ITER}+1))
    done
    ITER=0
    exec 0<&3
    MYDATA="${MYLINES[@]}"
    IFS=${ORIGIFS}
    echo "echoing FILE conetent "${MYDATA}""
}


_strip_ctrl_m()
{

    CTRL_M=$(echo ${MYDATA} | grep "^M")
    if [ -z "${CTRL_M}" ]; then
        return 0
    else
        echo "Found ^M characters... stripping them out."
        MYDATA=$(echo ${MYDATA} | sed "s/^M//g")
    fi

}


insert_vendor_secret()
{


  ## stuff the whole thing in the DB w/ mysql cmd line
  INSERT_QUERY="UPDATE tenant_extra_attributes_values SET value='${MYDATA}' WHERE master_id='5' AND tenant_id='1'"
  mysql -h $1 -u $2 -p$3 $4 \
         -e "${INSERT_QUERY}" > /dev/null

}


insert_vendor_code()
{

  INSERT_QUERY="UPDATE tenant_extra_attributes_values SET value='${MYDATA}' WHERE master_id='4' and tenant_id='1'"
  mysql -h $1 -u $2 -p$3 $4 \
         -e "${INSERT_QUERY}" > /dev/null
}



read_file_by_line ${1}
_strip_ctrl_m
file=$(basename $1)

if [ ${file} = "LicGenSecrets.xml" ]; then
   insert_vendor_secret $2 $3 $4 $5 $6
else
   insert_vendor_code $2 $3 $4 $5 $6
fi

