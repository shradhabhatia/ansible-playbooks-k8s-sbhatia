echo "Upgrading PIP ... "
pip install --upgrade pip
echo "Install Kube dontroller utility"
curl -o kubectl https://amazon-eks.s3-us-west-2.amazonaws.com/1.10.3/2018-07-26/bin/linux/amd64/kubectl
chmod +x ./kubectl
mkdir $HOME/bin && cp ./kubectl $HOME/bin/kubectl && export PATH=$HOME/bin:$PATH
echo 'export PATH=$HOME/bin:$PATH' >> ~/.bashrc
kubectl version --short --client
echo "Install IAM Authenticator ..."
curl -o aws-iam-authenticator https://amazon-eks.s3-us-west-2.amazonaws.com/1.10.3/2018-07-26/bin/linux/amd64/aws-iam-authenticator
chmod +x ./aws-iam-authenticator
cp ./aws-iam-authenticator $HOME/bin/aws-iam-authenticator && export PATH=$HOME/bin:$PATH
echo 'export PATH=$HOME/bin:$PATH' >> ~/.bashrc
aws-iam-authenticator help
echo "Check for Role to be used"
aws sts get-caller-identity
echo "Upgrade AWSCLI ..."
pip install awscli --upgrade --user
echo "Start using python 3.4 by default"
sudo update-alternatives --install /usr/bin/python python /usr/bin/python3.4 10
sudo update-alternatives --config python
sudo apt-get install python3-pip
sudo pip3 install --upgrade awscli
sudo update-alternatives --install /usr/bin/python python /usr/bin/python2.7 11
echo "Update kubernetes cluster ..."
aws eks update-kubeconfig --name gdev-cluster --region eu-west-1
kubectl get svc
sudo pip install openshift
